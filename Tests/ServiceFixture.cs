﻿using System.Threading.Tasks;
using BaseApiServices.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class AuthenticationServiceFixture
    {
        string username = "interview-test@drawboard.com";
        string pwd = "drawboard-test";

        [TestMethod]
        public async Task GivenValidCredentialsThenLoginSuccessful()
        {
            var apiService = new ApiServices.ApiServices();
            var user = await apiService.GetAccess(username, pwd);

            Assert.IsNotNull(user);
        }

        [TestMethod]
        public async Task GivenValidAccessTokenThenProjectsSuccessfullyRetrieved()
        {
            var apiService = new ApiServices.ApiServices();
            var loginDetails = await apiService.GetAccess(username, pwd);

            var scheme = loginDetails.AuthorizationHeader.Split(" ");
            loginDetails.AuthorizationScheme = scheme[0];

            var projectDetails = await apiService.GetUserProjects(loginDetails.AuthorizationScheme,
                loginDetails.AccessToken);

            Assert.IsNotNull(projectDetails);
        }

        [TestMethod]
        [ExpectedException(typeof(LoginFailedException))]
        public async Task GivenInvalidCredentialsThenLoginFailedException()
        {
            var apiService = new ApiServices.ApiServices();
            await apiService.GetAccess("xyz", pwd);
        }

        [TestMethod]
        [ExpectedException(typeof(LoginFailedException))]
        public async Task GivenInValidAccessTokenThenLoginFailedException()
        {
            var apiService = new ApiServices.ApiServices();
            var loginDetails = await apiService.GetAccess(username, pwd);
            await apiService.GetUserProjects("bearer",
                loginDetails.AuthorizationHeader);
        }
    }
}
