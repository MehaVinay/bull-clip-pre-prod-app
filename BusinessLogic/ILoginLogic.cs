﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApiServices.JsonDTO;

namespace BusinessLogic
{
    public interface ILoginLogic
    {
        Task SignIn(string username, string password);
        List<ProjectResponse> GetCachedProjects();
        UserLoginResponse GetUserDetails();
    }
}