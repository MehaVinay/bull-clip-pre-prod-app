﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApiServices;
using ApiServices.JsonDTO;
using BaseApiServices.Exceptions;
using MessageDialogService;
using Prism.Windows.Navigation;

namespace BusinessLogic
{
    public class LoginLogic : ILoginLogic
    {
        private readonly IApiServices apiServices;
        private UserLoginResponse loginDetails;
        private List<ProjectResponse> projectDetails;
        private readonly INavigationService navigationService;
        private readonly IModalDialogService messageDialogService;

        public LoginLogic(IApiServices api,
            INavigationService _navigationService,
            IModalDialogService _dialogService)
        {
            this.apiServices = api;
            this.navigationService = _navigationService;
            this.messageDialogService = _dialogService;
        }
        public async Task SignIn(string username, string password)
        {
            try
            {
                this.messageDialogService.ShowStatusAsync("Authenticating", "");
                loginDetails = await apiServices.GetAccess(username, password);
                var scheme = loginDetails.AuthorizationHeader.Split(" ");
                loginDetails.AuthorizationScheme = scheme[0];
                this.messageDialogService.HideStatusAsync();
                this.messageDialogService.ShowStatusAsync("Fetching User Projects", "");
                this.projectDetails = await apiServices.GetUserProjects(loginDetails.AuthorizationScheme,
                    loginDetails.AccessToken);
                this.messageDialogService.HideStatusAsync();
                navigationService.Navigate("Shell", null);
            }
            catch (InvalidRequestException exception)
            {
                await messageDialogService.ShowDialogAsync(exception.ReasonPhrase, exception.Message);
            }
            catch (LoginFailedException exception)
            {
                await messageDialogService.ShowDialogAsync(exception.ReasonPhrase, exception.Message);
            }
            catch (InvalidWebServerResponseException exception)
            {
                await messageDialogService.ShowDialogAsync(exception.ReasonPhrase, exception.Message);
            }
            catch (Exception exception)
            {
                await messageDialogService.ShowDialogAsync("Error", exception.Message);
            }
        }

        public List<ProjectResponse> GetCachedProjects()
        {
            return projectDetails;
        }

        public UserLoginResponse GetUserDetails()
        {
            return loginDetails;
        }
    }
}
