﻿using System;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace MessageDialogService
{
    public class ModalDialogService : IModalDialogService
    {
        private ContentDialog stautusDialog;

        public async Task ShowStatusAsync(string message, string title)
        {
            HideStatusAsync();
            if (string.IsNullOrEmpty(message))
            {
                message = "             ";
            }

            stautusDialog = new ContentDialog
            {
                Opacity = 0,
                BorderThickness = new Thickness(0),
                BorderBrush = new SolidColorBrush(Colors.Transparent),
                HorizontalContentAlignment = HorizontalAlignment.Center,
                Title = message + " " + "Please Wait...",
                Content = new ProgressRing
                {
                    IsActive = true
                },
                AccessKey = "ModalDialog"
            };
            await stautusDialog.ShowAsync();
        }

        public void HideStatusAsync()
        {
            stautusDialog?.Hide();
        }

        public async Task<object> ShowDialogAsync(string title, string message)
        {
            return await ShowDialogAsync(title, message, "OK");
        }

        public async Task<object> ShowDialogAsync(string title, string message, string buttonText)
        {
            var dialog = new ContentDialog
            {
                Title = title,
                Content = message,
                CloseButtonText = buttonText,
                AccessKey = "ModalDialog",
            };

            HideStatusAsync();
            return await dialog.ShowAsync();
        }
    }
}