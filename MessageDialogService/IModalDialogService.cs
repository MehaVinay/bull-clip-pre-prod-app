﻿using System.Threading.Tasks;

namespace MessageDialogService
{
    public interface IModalDialogService
    {
        Task ShowStatusAsync(string message, string title);
        void HideStatusAsync();
        Task<object> ShowDialogAsync(string title, string message);
    }
}