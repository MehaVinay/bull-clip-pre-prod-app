﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ApiServices.JsonDTO;
using BaseApiServices.Services;

namespace ApiServices.Services
{
    public class ProjectListService: BaseApiService<List<ProjectResponse>>
    {
        private const int ApiVersion = 1;

        public ProjectListService(TimeSpan _timeout,
                                  string _autherizationScheme,
                                  string _accessToken) : base(_timeout,  _autherizationScheme, _accessToken)
        {
        }

        protected override HttpContent GetContent()
        {
            return null;
        }

        protected override Dictionary<string, string> GetHeaders()
        {
            return new Dictionary<string, string>();
        }

        protected override HttpMethod GetRequestMethod()
        {
            return HttpMethod.Get;
        }

        protected override Uri GetRequestUri()
        {
          return new Uri($"https://preprod-api.bullclip.com/api/v{ApiVersion}/project/my");
        }

        public override async Task<List<ProjectResponse>> InitiateRequest()
        {
            return await this.SendAsync(true);
        }
    }
}
