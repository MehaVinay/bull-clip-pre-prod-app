﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ApiServices.JsonDTO;
using BaseApiServices.Services;

namespace ApiServices.Services
{
    public class UserLoginService : BaseApiService<UserLoginResponse>
    {
        private const int ApiVersion = 1;
        private readonly string password;
        private readonly string username;

        public UserLoginService(TimeSpan _timeout, string _username, string _password) : base(_timeout)
        {
            this.username = _username;
            this.password = _password;
        }

        protected override HttpContent GetContent()
        {
            var postData = new Dictionary<string, string>
            {
                ["username"] = username,
                ["password"] = password
            };

            return new FormUrlEncodedContent(postData);
        }

        protected override Dictionary<string, string> GetHeaders()
        {
            throw new NotImplementedException();
        }

        protected override HttpMethod GetRequestMethod()
        {
            throw new NotImplementedException();
        }

        protected override Uri GetRequestUri()
        {
            return new Uri($"https://preprod-api.bullclip.com/api/v{ApiVersion}/auth/login");
        }

        public override async Task<UserLoginResponse> InitiateRequest()
        {
            return await this.PostAsync();
        }
    }
}
