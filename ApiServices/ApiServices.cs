﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApiServices.JsonDTO;
using ApiServices.Services;

namespace ApiServices
{
    public class ApiServices : IApiServices
    {
        #region Public Methods
        public async Task<UserLoginResponse> GetAccess(string username, string pwd)
        {
            return await this.GetUserLoginService(username, pwd).InitiateRequest();
        }

        public async Task<List<ProjectResponse>> GetUserProjects(string autherizationScheme, string accessToken)
        {
            return await this.GetProjectListService(autherizationScheme, accessToken).InitiateRequest();
        }

        #endregion

        #region User Services

        private UserLoginService GetUserLoginService(string username, string pwd)
        {
            return new UserLoginService(TimeSpan.FromMinutes(2), username, pwd);
        }

        #endregion

        #region Project Services

        private ProjectListService GetProjectListService(string autherizationScheme, string accessToken)
        {
            return new ProjectListService(TimeSpan.FromMinutes(2), autherizationScheme, accessToken);
        }

        #endregion
    }
}
