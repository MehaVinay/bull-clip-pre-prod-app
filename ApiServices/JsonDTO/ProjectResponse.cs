﻿using Newtonsoft.Json;

namespace ApiServices.JsonDTO
{
    public class ProjectResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("permissions")]
        public string Permissions { get; set; }

        [JsonProperty("ownerId")]
        public string OwnerId { get; set; }

        [JsonProperty("createdDateUtc")]
        public string CreatedDateUtc { get; set; }

        [JsonProperty("deletedOn")]
        public string DeletedOn { get; set; }

        [JsonProperty("drawingCount")]
        public string DrawingCount { get; set; }

        [JsonProperty("documentCount")]
        public string DocumentCount { get; set; }

        [JsonProperty("userCount")]
        public string UserCount { get; set; }

        [JsonProperty("issuesCount")]
        public string IssuesCount { get; set; }

        [JsonProperty("organizationId")]
        public string OrganizationId { get; set; }
    }
}
