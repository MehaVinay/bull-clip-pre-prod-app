﻿using Newtonsoft.Json;

namespace ApiServices.JsonDTO
{
    public class UserLoginResponse
    {
        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }

        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }

        [JsonProperty("authorizationHeader")]
        public string AuthorizationHeader { get; set; }

        public string AuthorizationScheme { get; set; }

        [JsonProperty("expiresOnUtc")]
        public string ExpiresOnUtc { get; set; }

        [JsonProperty("userId")]
        public string UserId { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }
    }
}
