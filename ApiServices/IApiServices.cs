﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApiServices.JsonDTO;

namespace ApiServices
{
    public interface IApiServices
    {
        Task<UserLoginResponse> GetAccess(string username, string pwd);
        Task<List<ProjectResponse>> GetUserProjects(string autherizationScheme, string accessToken);
    }
}