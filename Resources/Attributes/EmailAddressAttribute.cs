﻿using System.ComponentModel.DataAnnotations;

namespace Resources.Attributes
{
    public class CustomEmailAddressAttribute : ValidationAttribute
    {
        public const string _regex_pattern =
            @"^(?("")("".+?(?<!\\)""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
            @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-0-9a-zA-Z]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9][\-a-zA-Z0-9]{0,22}[a-zA-Z0-9]))$";

        public override bool IsValid(object _value)
        {
            if (_value == null)
            {
                return true;
            }
            var attribute = new RegularExpressionAttribute(_regex_pattern);
            if (attribute.IsValid(_value)) return true;
            return false;
        }
    }
}