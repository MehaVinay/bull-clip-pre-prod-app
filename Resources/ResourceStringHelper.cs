using System;
using Windows.ApplicationModel.Resources;

namespace Resources
{
    public static class ResourceStringHelper
    {
        private static readonly ResourceLoader ResourceLoader = ResourceLoader.GetForViewIndependentUse();
        public static string RequiredErrorMessage => "RequiredString".GetLocalizedString();
        public static string InvalidEmailAddressFormatErrorMessage => "InvalidEmailAddressFormat".GetLocalizedString();
        public static string GetLocalizedString(this string stringname)
        {
            try
            {
                return ResourceLoader.GetString(stringname);
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}
