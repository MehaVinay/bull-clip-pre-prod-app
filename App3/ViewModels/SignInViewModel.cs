﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using BusinessLogic;
using Prism.Commands;
using Prism.Windows.Navigation;
using Prism.Windows.Validation;
using Resources;
using Resources.Attributes;

namespace App3.ViewModels
{
    public class SignInViewModel : ValidatableBindableBase
    {
        private string emailAddress;
        private string password;
        private readonly ILoginLogic loginLogic;
        
        public DelegateCommand ControlCommand { get; }

        public SignInViewModel(ILoginLogic _loginLogic)
        {
            this.loginLogic = _loginLogic;
            this.ControlCommand = new DelegateCommand(async () => await SignIn());
        }

        [CustomEmailAddress(ErrorMessageResourceType = typeof(ResourceStringHelper), ErrorMessageResourceName = "InvalidEmailAddressFormatErrorMessage")]
        public string EmailAddress
        {
            get => emailAddress;
            set
            {
                this.SetProperty(ref emailAddress, value);
                this.RaisePropertyChanged("IsValid");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(ResourceStringHelper), ErrorMessageResourceName = "RequiredErrorMessage")]
        public string Password
        {
            get => password;
            set
            {
                SetProperty(ref password, value);
                this.RaisePropertyChanged("IsValid");
            }
        }

        public bool IsValid => this.ValidateProperties();

        public async Task SignIn()
        {
            await this.loginLogic.SignIn(this.emailAddress, this.password);
        }

        public string ScreenName => "SignInViewScreenName".GetLocalizedString();
        public string ViewControlCommandContent => "SignInViewScreenNameControlBtn".GetLocalizedString();
    }
}
