﻿using System;
using System.Collections.Generic;
using ApiServices.JsonDTO;
using App3.Constants;
using BusinessLogic;
using Prism.Commands;
using Prism.Windows.Mvvm;
using Prism.Windows.Navigation;

namespace App3.ViewModels
{
    public class ShellViewModel : ViewModelBase
    {
        private List<ProjectResponse> projects;
        private UserLoginResponse userDetails;
        private readonly INavigationService navigationService;
        public DelegateCommand HamburgerButtonCommand { get; }
        public DelegateCommand SignOutCommand { get; }

        private bool _isPaneOpen = false;
        public bool IsPaneOpen
        {
            get => _isPaneOpen;
            set
            {
                _isPaneOpen = value;
                this.RaisePropertyChanged();
            }
        }
        private void OpenPane()
        {
            this.IsPaneOpen = _isPaneOpen == false;
        }

        public ShellViewModel(INavigationService _navigationService,
                              ILoginLogic logic)
        {
            this.navigationService = _navigationService;
            projects = logic.GetCachedProjects();
            userDetails = logic.GetUserDetails();
            this.HamburgerButtonCommand = new DelegateCommand(OpenPane, () => true);
            this.SignOutCommand = new DelegateCommand(SignOut);
        }

        public IList<ProjectResponse> SubMenuItems
        {
            get { return projects; }
        }

        public UserLoginResponse UserDetails
        {
            get { return userDetails; }
        }

        public override async void OnNavigatedTo(NavigatedToEventArgs e, Dictionary<string, object> viewModelState)
        {
            navigationService.ClearHistory();
        }

        private async void SignOut()
        {
            try
            {
                this.navigationService.Navigate(PageTokens.SignIn,null);
            }
            catch (Exception e)
            {
            }
        }
    }
}
