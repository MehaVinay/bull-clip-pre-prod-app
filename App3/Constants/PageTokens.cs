﻿namespace App3.Constants
{
    internal static class PageTokens
    {
        public const string MainPage = "Main";
        public const string SignIn = "SignIn";
        public const string SettingsPage = "Settings";
    }
}
