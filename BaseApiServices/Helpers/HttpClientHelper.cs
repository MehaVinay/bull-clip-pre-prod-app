﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using BaseApiServices.Exceptions;

namespace BaseApiServices.Helpers
{
    public class HttpClientHelper: IDisposable
    {
        #region Properties
        private readonly HttpClient client;
        private readonly TimeSpan timeout;
        
        #endregion

        #region Constructors

        public HttpClientHelper(TimeSpan _timeout)
        {
            this.client = new HttpClient();
            this.timeout = _timeout;
        }

        #endregion

        #region Public Methods

        public async Task<HttpResponseMessage> PostRequestAsync(string uri, HttpContent content)
        {
            using (var source = new CancellationTokenSource(this.timeout))
            {
                try
                {
                    return await client.PostAsync(uri, content, source.Token).ConfigureAwait(false);
                }
                catch (System.Runtime.InteropServices.COMException comException)
                {
                    throw new InvalidRequestException(comException.Message, comException);
                }
                catch (OperationCanceledException oce)
                {
                    throw new TimeoutException("Connection timeout", oce);
                }
                catch (LoginFailedException)
                {
                    throw;
                }
                catch (Exception exception)
                {
                    if (exception.Message.Contains("The text associated with this error code could not be found."))
                    {
                        throw new InvalidRequestException(exception.Message, exception);
                    }

                    throw;
                }
            }
        }

        public async Task<HttpResponseMessage> SendRequestAsync(HttpRequestMessage _request)
        {
            using (var source = new CancellationTokenSource(this.timeout))
            {
                try
                {
                    return await this.client.SendAsync(_request, source.Token).ConfigureAwait(false);
                }
                catch (System.Runtime.InteropServices.COMException comException)
                {
                    throw new InvalidRequestException(comException.Message, comException);
                }
                catch (OperationCanceledException oce)
                {
                    throw new TimeoutException("Connection timeout", oce);
                }
            }
        }

        #endregion

        public async void Dispose()
        {
            try
            {
                this.client.Dispose();
            }
            catch (ObjectDisposedException)
            {
                
            }
        }

    }
}
