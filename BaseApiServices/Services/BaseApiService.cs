﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using BaseApiServices.Exceptions;
using BaseApiServices.Helpers;
using BaseApiServices.JsonDTO;
using Newtonsoft.Json;

namespace BaseApiServices.Services
{
    public abstract class BaseApiService<T>
    {
        private readonly TimeSpan timeout;

        protected HttpRequestMessage CreateRequestMessage(bool _withAccessToken = false)
        {
            var request = new HttpRequestMessage
            {
                Method = this.GetRequestMethod(),
                RequestUri = this.GetRequestUri(),
                Content = this.GetContent()
            };

            foreach (var header in this.GetHeaders())
                request.Headers.Add(header.Key, header.Value);
            
            request.Headers.Authorization = this.GetAuthenticationHeader();
            return request;
        }

        protected async Task<T> PostAsync()
        {
            using (var client = this.CreateClient())
            {
                try
                {
                    using (var resp = await client.PostRequestAsync(this.GetRequestUri().AbsoluteUri, GetContent())
                        .ConfigureAwait(false))
                    {
                        return await this.HandleResponseAsync(resp)
                            .ConfigureAwait(false);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        protected virtual async Task<T> SendAsync(bool _requestWithAccessToken = false)
        {
            try
            {
                using (var client = this.CreateClient())
                {
                    using (var req = this.CreateRequestMessage(_requestWithAccessToken))
                    {
                        using (var resp = await client.SendRequestAsync(req)
                            .ConfigureAwait(false))
                        {
                            return await this.HandleResponseAsync(resp)
                                .ConfigureAwait(false);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        protected async Task<T> HandleResponseAsync(HttpResponseMessage resp)
        {
            if (resp.StatusCode == HttpStatusCode.OK)
            {
                var input = await resp.Content.ReadAsStreamAsync().ConfigureAwait(false);

                using (var stream = input)
                {
                    using (var sr = new StreamReader(stream))
                    {
                        using (var reader = new JsonTextReader(sr))
                        {
                            var serializer = new JsonSerializer();
                            return serializer.Deserialize<T>(reader);
                        }
                    }
                }
            }

            var message = await resp.Content.ReadAsStringAsync().ConfigureAwait(false);

            if (resp.StatusCode == HttpStatusCode.Unauthorized
               ||
               resp.StatusCode == HttpStatusCode.BadRequest)
            {
                throw new LoginFailedException(message, resp.StatusCode.ToString(), resp.ReasonPhrase);
            }

            throw new InvalidWebServerResponseException(message,
                resp.StatusCode.GetHashCode(), resp.ReasonPhrase);
        }

        private HttpClientHelper CreateClient()
        {
            return new HttpClientHelper(this.timeout);
        }

        #region Constructors

        protected BaseApiService(TimeSpan _timeout,
                                string _autherizationScheme,
                                string _accessToken)
        {
            this.timeout = _timeout;
            this.AutherizationScheme = _autherizationScheme;
            this.AccessToken = _accessToken;
        }

        protected BaseApiService(TimeSpan _timeout) : this(_timeout,null, null)
        {
        }

        #endregion

        #region Fields

        protected string AutherizationScheme { get; }
        protected string AccessToken { get; }
        #endregion

        #region Abstract Methods
        
        protected virtual AuthenticationHeaderValue GetAuthenticationHeader()
        {
            return new AuthenticationHeaderValue(AutherizationScheme, AccessToken);
        }

        protected abstract HttpContent GetContent();

        protected abstract Dictionary<string, string> GetHeaders();
        
        protected abstract HttpMethod GetRequestMethod();
        protected abstract Uri GetRequestUri();
        public abstract Task<T> InitiateRequest();

        #endregion
    }
}