﻿using System;

namespace BaseApiServices.Exceptions
{
    public class InvalidWebServerResponseException : BaseException
    {
        public int ResponseStatusCode { get; }
        public string ReasonPhrase { get; }

        public InvalidWebServerResponseException(string message) : base(message)
        {
        }

        public InvalidWebServerResponseException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public InvalidWebServerResponseException
            (string message, int statusCode, string reasonPhrase)
            : this(message, statusCode, reasonPhrase, null)
        {
        }

        public InvalidWebServerResponseException(string message, int statusCode,
            string reasonPhrase, Exception innerException)
            : base(message, innerException)
        {
            this.ResponseStatusCode = statusCode;
            this.ReasonPhrase = reasonPhrase;
        }
    }
}
