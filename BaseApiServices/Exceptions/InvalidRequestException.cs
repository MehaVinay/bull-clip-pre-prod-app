﻿using System;

namespace BaseApiServices.Exceptions
{
    public class InvalidRequestException: BaseException
    {
        public int ResponseStatusCode { get; }
        public string ReasonPhrase { get; }

        public InvalidRequestException(string message) :base(message)
        {
        }

        public InvalidRequestException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public InvalidRequestException
            (string message, int statusCode, string reasonPhrase)
            : this(message, statusCode, reasonPhrase, null)
        {
        }

        public InvalidRequestException(string message, int statusCode,
            string reasonPhrase, Exception innerException)
            : base(message, innerException)
        {
            this.ResponseStatusCode = statusCode;
            this.ReasonPhrase = reasonPhrase;
        }
    }
}
