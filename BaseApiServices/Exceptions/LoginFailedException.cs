﻿using System;

namespace BaseApiServices.Exceptions
{
    public class LoginFailedException : BaseException
    {
        public LoginFailedException(string message) : base(message)
        {
        }
        
        public LoginFailedException
            (string message, string statusCode, string reasonPhrase)
            : this(message, statusCode, reasonPhrase, null)
        {
        }

        public LoginFailedException(string message, string statusCode,
            string reasonPhrase, Exception innerException)
            : base(message, innerException)
        {
            this.ResponseStatusCode = statusCode;
            this.ReasonPhrase = reasonPhrase;
        }

        public string ResponseStatusCode { get; private set; }
        public string ReasonPhrase { get; private set; }
    }
}
