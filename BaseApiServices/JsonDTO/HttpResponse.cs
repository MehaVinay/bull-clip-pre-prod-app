﻿using Newtonsoft.Json;

namespace BaseApiServices.JsonDTO
{
    public class HttpResponse
    {
        [JsonProperty("message")]
        public string Message { get; set; }
        
        [JsonProperty("StatusCode")]
        public string StatusCode { get; set; }

        [JsonProperty("ReasonPhrase")]
        public string ReasonPhrase { get; set; }

        [JsonProperty("errors")]
        public string Errors { get; set; }
    }
}
